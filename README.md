# Rust DLL framework
DLL boiler plate code, in Rust. Includes logging and error handling.

To get started, determine your use-case. Then, in `src/lib.rs` replace the function passed to `start_thread` in `DllMain`.

## One-shot code execution
Can be found in `src/thread/main_fns/cmd_exec/mod.rs`. As implied, some code is executed and the DLL will be unloaded immediately after that. By default it invokes `whoami` in PowerShell.

## Looping evaluation
Starts a "main loop". By default it checks user-input. If RControl is pressed, the DLL unloads itself.