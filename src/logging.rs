use std::path::PathBuf;

use anyhow::{anyhow, Context};
use litcrypt::lc;
use log::LevelFilter;
#[cfg(debug_assertions)] // debug only
use log4rs::append::console::ConsoleAppender;
use log4rs::{
    append::file::FileAppender,
    config::{Appender, Root},
    encode::pattern::PatternEncoder,
    Config,
};
use once_cell::sync::Lazy;
#[cfg(debug_assertions)] // debug only
use winapi::um::consoleapi::AllocConsole;
#[cfg(debug_assertions)] // debug only
use winapi::um::wincon::FreeConsole;

#[cfg(debug_assertions)] // debug only
use crate::win_error::WinError;

static LOG_PATTERN: Lazy<String> =
    Lazy::new(|| lc!("{d(%Y-%m-%d %H:%M:%S %Z)(utc)} - P{P:<6}T{I:<6} - {h({l})}: {m}{n}"));
static FILE_LOGGER_NAME: Lazy<String> = Lazy::new(|| lc!("file_logger"));
#[cfg(debug_assertions)] // debug only
static CONSOLE_LOGGER_NAME: Lazy<String> = Lazy::new(|| lc!("console.logger"));

pub(super) fn init_logging() -> Result<(), anyhow::Error> {
    let pattern = LOG_PATTERN.as_str();
    let pattern = Box::new(PatternEncoder::new(pattern));

    let file_logger = FileAppender::builder()
        .encoder(pattern.clone())
        .build(
            log_file()
                .to_str()
                .ok_or_else(|| anyhow!(lc!("Invalid log file path")))?,
        )
        .unwrap();
    #[allow(unused_mut)] // debug only
    let mut config_builder = Config::builder()
        .appender(Appender::builder().build(FILE_LOGGER_NAME.as_str(), Box::new(file_logger)));
    #[allow(unused_mut)] // debug only
    let mut root_builder = Root::builder().appender(FILE_LOGGER_NAME.as_str());
    #[allow(unused_mut)] // debug only
    #[allow(unused_assignments)] // debug only
    let mut log_level = LevelFilter::Info;

    #[cfg(debug_assertions)]
    {
        unsafe {
            AllocConsole();
        };
        let console_logger = ConsoleAppender::builder().encoder(pattern).build();
        config_builder = config_builder.appender(
            Appender::builder().build(CONSOLE_LOGGER_NAME.as_str(), Box::new(console_logger)),
        );
        root_builder = root_builder.appender(CONSOLE_LOGGER_NAME.as_str());
        log_level = LevelFilter::Debug;
    };

    log4rs::init_config(config_builder.build(root_builder.build(log_level))?)
        .map(|_| ())
        .context(lc!("Logger initialization failed"))
}

pub(super) fn deinit_logging() -> Result<(), anyhow::Error> {
    #[cfg(debug_assertions)]
    if unsafe { FreeConsole() } == 0 {
        return Err(anyhow!("{:?}", WinError::new()).context(lc!("Clean-up of console failed")));
    }

    Ok(())
}

fn log_file() -> PathBuf {
    let mut tmp_dir = std::env::temp_dir();
    tmp_dir.push(env!("CARGO_PKG_NAME").to_owned() + &lc!(".log"));
    tmp_dir
}
