mod logging;
mod thread;
mod win_error;

use std::sync::atomic::AtomicPtr;

use anyhow::{anyhow, Context};
use litcrypt::{lc, use_litcrypt};
use log::{debug, error, info};
use once_cell::sync::OnceCell;
use winapi::{
    shared::{
        minwindef::{BOOL, DWORD, FALSE, HINSTANCE, LPVOID, TRUE},
        ntdef::HANDLE,
    },
    um::{
        libloaderapi::FreeLibraryAndExitThread,
        winnt::{DLL_PROCESS_ATTACH, DLL_PROCESS_DETACH, DLL_THREAD_ATTACH, DLL_THREAD_DETACH},
    },
};

use crate::{
    logging::{deinit_logging, init_logging},
    thread::{main_fns, start_thread, stop_thread},
};

use_litcrypt!();
static mut HACK_THREAD: OnceCell<AtomicPtr<HANDLE>> = OnceCell::new();

#[no_mangle]
extern "system" fn DllMain(dll_module: HINSTANCE, call_reason: DWORD, reserved: LPVOID) -> BOOL {
    fn dll_main_result_wrapper(
        dll_module: HINSTANCE,
        call_reason: DWORD,
        _: LPVOID,
    ) -> Result<(), anyhow::Error> {
        match call_reason {
            DLL_PROCESS_ATTACH => {
                init_logging()?;
                info!("{}", lc!("Process attached; initializing"));

                // TODO: choose the correct main fn module here
                let mut started_thread =
                    start_thread(Some(main_fns::cmd_exec::internal_main), dll_module)
                        .context(lc!("Init of inner main thread fn failed"))?;

                unsafe { HACK_THREAD.set(AtomicPtr::from(&mut started_thread as *mut _)) }
                    .map_err(|_| anyhow!(lc!("Saving handle to inner main thread failed")))?;
            }
            DLL_PROCESS_DETACH => {
                info!("{}", lc!("Process detached; cleaning up"));

                let thread_cleanup = unsafe { HACK_THREAD.take() }
                    .ok_or_else(|| anyhow!(lc!("Handle to hack thread not taken")))
                    .and_then(|thread| stop_thread(unsafe { *thread.into_inner() }));
                let logging_cleanup = deinit_logging();

                if let Err(e) = thread_cleanup.and(logging_cleanup) {
                    error!("{:?}", e);
                }
                // DLL already cleanup up in custom thread. Don't don't free!
                // dll_exit(dll_module);
            }
            DLL_THREAD_ATTACH => debug!("{}", lc!("Thread attached")),
            DLL_THREAD_DETACH => debug!("{}", lc!("Thread detached")),
            _ => {
                // Official possible DllMain call reasons: https://learn.microsoft.com/en-us/windows/win32/dlls/dllmain
                error!("{}",lc!("All official DllMain call reasons have been checked. Did you invoke this with 'rundll'?"));
            }
        }

        Ok(())
    }

    match dll_main_result_wrapper(dll_module, call_reason, reserved) {
        Ok(_) => TRUE,
        Err(e) => {
            error!("{:?}", e);
            FALSE
        }
    }
}

fn dll_exit(dll_module: HINSTANCE) {
    unsafe {
        FreeLibraryAndExitThread(dll_module, 0);
    };
}
