mod user_input;

use std::mem;

use litcrypt::lc;
use log::{debug, error, info};
use winapi::shared::minwindef::LPVOID;

use crate::{dll_exit, logging::deinit_logging};
use user_input::UserInput;

#[allow(dead_code)]
pub(crate) extern "system" fn internal_main(dll_module: LPVOID) -> u32 {
    info!("{}", lc!("IN INTERNAL MAIN"));

    // pseudo-main loop
    let mut user_input = UserInput::default();
    while !user_input.eject_dll() {
        user_input.update();

        debug!("{}", lc!("Just updated user input!"));
    }

    if let Err(e) = deinit_logging() {
        error!("{:?}", e);
    }
    dll_exit(unsafe { mem::transmute(dll_module) });
    0
}
