use getset::Getters;
use winapi::um::winuser::{GetAsyncKeyState, VK_RCONTROL};

#[derive(Default, Getters)]
pub(super) struct UserInput {
    #[getset(get = "pub(super)")]
    eject_dll: bool,
}

impl UserInput {
    pub(super) fn update(&mut self) -> &mut Self {
        self.eject_dll = unsafe { GetAsyncKeyState(VK_RCONTROL) } != 0;

        self
    }
}
