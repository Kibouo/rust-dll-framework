use std::mem;

use litcrypt::lc;
use log::{error, info};
use winapi::shared::minwindef::LPVOID;

use crate::{dll_exit, logging::deinit_logging};

#[allow(dead_code)]
pub(crate) extern "system" fn internal_main(dll_module: LPVOID) -> u32 {
    info!("{}", lc!("IN INTERNAL MAIN"));

    let result = std::process::Command::new("powershell.exe")
        .arg("whoami /priv")
        .status();
    match result {
        Ok(status) => {
            let code = status.code().unwrap_or(1);
            if code != 0 {
                return code as u32;
            }
        }
        Err(_) => return 1,
    }

    if let Err(e) = deinit_logging() {
        error!("{:?}", e);
    }
    dll_exit(unsafe { mem::transmute(dll_module) });
    0
}
