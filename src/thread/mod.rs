pub(super) mod main_fns;

use std::{mem, ptr};

use anyhow::anyhow;
use litcrypt::lc;
use winapi::{
    shared::{minwindef::HINSTANCE, ntdef::HANDLE},
    um::{
        handleapi::CloseHandle, minwinbase::LPTHREAD_START_ROUTINE, processthreadsapi::CreateThread,
    },
};

use crate::win_error::WinError;

pub(super) fn start_thread(
    init_function: LPTHREAD_START_ROUTINE,
    dll_module: HINSTANCE,
) -> Result<HANDLE, anyhow::Error> {
    let created_thread = unsafe {
        CreateThread(
            ptr::null_mut(),
            0,
            init_function,
            mem::transmute(dll_module),
            0,
            ptr::null_mut(),
        )
    };

    if created_thread.is_null() {
        return Err(anyhow!("{:?}", WinError::new()).context(lc!("Starting of thread failed")));
    }

    Ok(created_thread)
}

pub(super) fn stop_thread(thread: HANDLE) -> Result<(), anyhow::Error> {
    if unsafe { CloseHandle(thread) } == 0 {
        return Err(anyhow!("{:?}", WinError::new()).context(lc!("Clean-up of thread failed")));
    }

    Ok(())
}
